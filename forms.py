from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

class CommandForm(forms.Form):

  def __init__(self, *args, **kwargs):
    super(CommandForm, self).__init__(*args, **kwargs)
    self.helper = FormHelper()
    self.helper.form_action = 'api'

    self.helper.add_input(Submit('submit', 'Submit'))


  username = forms.CharField(max_length=100)
  password = forms.CharField(max_length = 100, widget=forms.PasswordInput)
  command = forms.CharField(max_length = 100)
  chain = forms.CharField(max_length = 100, required = False)
  arguments = forms.CharField(max_length = 100, required = False,
    widget=forms.Textarea)
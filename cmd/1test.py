import re
import ast

def log(info):
	pass
class UnknownResponseError(RuntimeError):
    '''Raised when the response from a multichain command cannot be understood'''

class MultiChainCMDCaller():

	def __init__(self, cmd):
		self.cmd = cmd
		pass


class MultiChainCMDReader():

	def __init__(self):
		self.head = None
		self.body = None
		self.type = ''

		

	def read(self, resp):
		self.resp = str(resp)
		if self.read_head():
			self.read_body()

	def read_head(self):
		"""
		Attempts to find the message 'head' in the response from multichain.
		"""

		
		m = re.search('{+.+}', self.resp)
		if m:
			m = ast.literal_eval(m.group(0))
			self.type = 'SUCCESS'
			self.head = m
			return m

		m = re.search('error:.*', self.resp, re.I)
		if m:
			m = m.group(0)
			self.type = 'ERROR'
			self.body = self.resp
			self.head = m
			return m

		m = re.search('Network parameter set was successfully generated', self.resp)
		if m:
			m = m.group(0)
			self.type = 'SUCCESS'
			self.body = self.resp
			self.head = m
			return m

		raise UnknownResponseError('The response from multichain was not recognized:\n %s' % self.resp)

	def read_body(self):
		"""
		Attempts to find dictionaries of information in the response from multichain.
		"""

		m = re.search('\[\n.*{.*\]', self.resp, re.DOTALL)
		if m:
			m = ast.literal_eval(m.group(0))[0] #Evaluate and extract Dict
			self.body = m
			return m


		start = [i for i, char in enumerate(self.resp) if char == '{']
		end = [i for i, char in enumerate(self.resp) if char == '}']
		self.resp = self.resp.replace('true', 'True')
		self.resp = self.resp.replace('false', 'False')
		print ast.literal_eval(self.resp[start[1]:end[1]+1])

		m = re.search('{\n.*}', self.resp, re.DOTALL)
		if m:
			m = m.group(0)
			m = m.replace('true', 'True')
			m = m.replace('false', 'False') #Evaluate and extract Dict
			m = ast.literal_eval(m)
			self.body = m
			return m


files = ['test_error.txt', 'test_resp.txt', 'blockain_exists_already.txt', 'blockchain_created.txt']


with open('getinfo_call.txt') as f:
	txt = f.read()
	a = MultiChainCMDReader()
	a.read(txt)
	print a.head
	print a.body



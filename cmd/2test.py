import ast
import socket
import sys
import select

class UDPClient():

	def __init__(self, name):
		self.name = name
		self.HOST, self.PORT = "128.199.224.200", 1000
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.sock.setblocking(0)
		self.sock.settimeout(3)

	def newchain(self):

		try:
			#Create the chain
			cmd = self.name + ' create'
			resp = self.send_recv(cmd)

			print resp

		except:
			return 'Error creating chain'

		try:
			#Inititalize the chain...
			cmd = self.name + ' initialize_chain'
			resp = self.send_recv(cmd)
			print resp
		except:
			return 'Error initializing chain'

		try:
			return self.issue('usd', 1000000000000, 0.01)
		except:
			return 'Error setting up initial balance.'

	def issue(self, asset_name, units, divisions):
		"""
		Issues new assets, allowing one to select the name, total number of units and
		how the units can be divided.
		"""

		units = int(units)
		divisions = float(divisions)

		#Get the account with permissions to issue assets...
		cmd = self.name + ' issueaccount'
		resp = self.send_recv(cmd)

		for account_dict in resp:
			if account_dict['address']:
				issuing_account = account_dict['address']
				cmd = '{0} issue {1} {2} {3} {4}'.format(self.name, issuing_account, asset_name, units, divisions)
				return self.send_recv(cmd)


		return resp



	def send_recv(self, data):
		"""
		Sends a request through the socket to the server and retrieves the response,
		converting it to a python object in the process.
		"""

		reps = 0
		while reps < 3:
			self.sock.sendto(data, (self.HOST, self.PORT))
			r, _, _ = select.select([self.sock], [], [], 3)
			if r:
				# ready to receive
				response = self.sock.recv(1024*4)
				try:
					return ast.literal_eval(response)
				except SyntaxError:
					return response

			reps += 1

		return 'Could not reach server!'

	def run(self, cmd = None):
		"""
		Parses and identifies the command to use from the command line
		and determines whether the command is defined in this class
		or should be passed on as-is
		"""

		if sys.argv[1]:
			name = sys.argv[1]
			cmd = " ".join(sys.argv[1:]) #Build command from command line arguments

			if len(sys.argv) > 3:
				args = sys.argv[3:]
			else:
				args = None

		elif cmd:

			return str(cmd)


		
		#See if the command used is defined in this class...
		try:
			if args:
				#If there are args, pass them to the called function as parameters
				return getattr(self, sys.argv[2])(*args)
			else:
				#If not, call the function without any parameters...
				return getattr(self, sys.argv[2])()
		except:
			pass			

		#If the command is not defined in the class, pass it along as is...
		return self.send_recv(cmd)

if __name__ == "__main__":
	c = UDPClient(sys.argv[1])
	print c.run()
import ast
import socket
import sys
import select

class UDPClient():

	def __init__(self, name, *args):
		self.name = name
		self.HOST, self.PORT = "128.199.224.200", 1000
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.sock.setblocking(0)
		self.sock.settimeout(3)
		self.args = args

	##Functions for creating chain and getting basic information.
	def create(self):
		cmd = 'multichain-util clone test101 %s' % self.name
		return self.send_recv(cmd)

	def initialize(self):
		cmd = 'multichaind %s -daemon' % self.name
		return self.send_recv(cmd)

	def getinfo(self):
		cmd = 'multichain-cli %s getinfo' % self.name
		return self.send_recv(cmd)

	def listpermissions(self):
		cmd = 'multichain-cli {0} listpermissions {1}'.format(self.name, self.args[0])
		return self.send_recv(cmd)

	def newchain(self):

		try:
			self.create()
		except:
			return 'Error creating chain'

		try:
			self.initialize()
		except:
			return 'Error initializing chain'
		
		return self.getinfo()
	



	def sendto(self, from_acc = None, to_acc = None, amount = None, asset = None):

		if from_acc == None:
			from_acc = self.issueaccount()
			from_acc = from_acc[0]['address']

		
		cmd = 'multichain-cli {0} sendfromaddress {1} {2}'.format(self.name, from_acc, to_acc)

		val = '{"USD":%s}' % amount #added in 2 steps to avoid trying to escape single quotes from string
		cmd = cmd +  " '{0}'".format(val)

		return self.send_recv(cmd)

	def getaddressbalances(self, address):
		
		cmd = 'multichain-cli {0} getaddressbalances {1}'.format(self.name, address)
		return self.send_recv(cmd)

	def grantpermissions(self):
		pass

	def issueaccount(self):
		""" Retrieve the account with issuing rights.
		"""

		cmd = 'multichain-cli %s listpermissions issue' % self.name
		resp = self.send_recv(cmd)
		if type(resp[0]) == type({'dict':'dict'}):
			return resp[0]['address']
		return resp

	def getaddresses(self):
		cmd = 'multichain-cli %s getaddresses' % self.name
		return self.send_recv(cmd)

	def getassetbalances(self):
		cmd = 'multichain-cli %s getassetbalances' % self.name
		return self.send_recv(cmd)


	def newaccount(self):

		cmd = 'multichain-cli %s getnewaddress' % self.name
		resp = self.send_recv(cmd)

		#Extract new account's ID from the response
		acc_index = resp.find('}')
		if acc_index == -1:
			return 'Error creating account.'
		acc_name = resp[acc_index+1:].strip()
		
		cmd = 'multichain-cli {0} grant {1} receive,send'.format(self.name, acc_name)
		return self.send_recv(cmd)


	def issue(self, asset_name, units, divisions):
		"""
		Issues new assets, allowing one to select the name, total number of units and
		how the units can be divided.
		"""

		units = int(units)
		divisions = float(divisions)

		#Get the account with permissions to issue assets...
		issuing_account = self.issueaccount()
		cmd = 'multichain-cli {0} issue {1} {2} {3} {4}'.format(self.name, issuing_account, asset_name, units, divisions)
		return self.send_recv(cmd)


	def send_recv(self, data):
		"""
		Sends a request through the socket to the server and retrieves the response,
		converting it to a python object in the process.
		"""

		reps = 0
		while reps < 3:
			self.sock.sendto(data, (self.HOST, self.PORT))
			r, _, _ = select.select([self.sock], [], [], 3)
			if r:
				# ready to receive
				response = self.sock.recv(1024*4)
				try:
					return ast.literal_eval(response)
				except SyntaxError:
					return response

			reps += 1

		return 'Could not reach server!'

	def run(self, cmd = None):
		"""
		Parses and identifies the command to use from the command line
		and determines whether the command is defined in this class
		or should be passed on as-is
		"""

		#When called programmatically...
		if cmd:
			return self.send_recv(cmd)

		#When called from the command line...
		elif sys.argv[1]:
			cmd = " ".join(sys.argv[1:]) #Build command from command line arguments

			if len(sys.argv) > 3:
				args = sys.argv[3:]
			else:
				args = None

		#If the command used is defined in this class...
		try:
			if args:
				return getattr(self, sys.argv[2])(*args)
			else:
				return getattr(self, sys.argv[2])()
		except:
			pass			

		#If the command is not defined in the class, and is called from the
		#command line...
		cmd = " ".join(sys.argv[1:])
		return self.send_recv(cmd)

if __name__ == "__main__":
	c = UDPClient(sys.argv[1])
	print c.run()
import pyexcel as pe
import pyexcel.ext.xls # import it to handle xls file
import pyexcel.ext.xlsx

records = pe.get_records(file_name="dummy.xlsx")
for record in records:
	print("%s is aged at %d" % (record['Name'], record['Age']))
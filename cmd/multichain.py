import datetime
from pexpect import run, spawn, EOF, TIMEOUT
from subprocess import Popen, PIPE
import re
import ast
import time

def log(info):
	pass

class UnknownResponseError(OSError):
    '''Raised when the response from a multichain command cannot be understood'''

class ChainDoesNotExistError(OSError):
    '''Raised when the response from a multichain command cannot be understood'''


class ReaderSignal():

	def __init__(self, user_msg, code, next_action = None):
		self.user_msg = user_msg
		self.cmd_response = cmd_response
		self.next_action = next_action


class BaseReader():
	"""
	Base class for all readers. A reader parses the JSON returned by calls to multichain
	to determine whether or not the call was successfully completed.

	"""

	def __init__(self, caller):
		self.caler = caller
		self.name = caller.name
		self.head = None
		self.payload = None
		self.resp = None

	def read(self, resp):
		print type(resp)
		self.resp = str(resp)

		if self.throws_error():
			return self.handle_error()
		else:
			self.set_head()
			self.set_body()
			return self.handle_event()


	def set_head(self):
		"""
		Looks for the 'head' of a multichain command line call. The head is a dictionary
		at the beginning of a call with detail information about the call.
		"""

		m = re.search('{+.+}', self.resp)
		if m:
			m = ast.literal_eval(m.group(0))
			m['time_stamp'] = datetime.datetime.now()
			self.head = m
			return True
		return False

	def set_payload(self):

		"""
		Attempts to find dictionaries of information in the response from multichain.
		"""

		m = re.search('\[\n.*{.*\]', self.resp, re.DOTALL)
		if m:
			m = ast.literal_eval(m.group(0))[0] #Evaluate and extract Dict
			self.payload = m
			return self.payload

		
		#If the body returned is a simple json dict...
		if self.head:
			try:
				self.resp = self.resp.replace('true', 'True')
				self.resp = self.resp.replace('false', 'False')
				start = [i for i, char in enumerate(self.resp) if char == '{']
				end = [i for i, char in enumerate(self.resp) if char == '}']			
				self.payload = ast.literal_eval(self.resp[start[1]:end[1]+1])

				return self.payload
			except:
				pass

		return False

	def throws_error(self):
		"""
		Identifies whether or not multichain call returned an error message.

		"""

		if 'error:' in self.resp:
			return True
		else:
			return False

	def handle_error(self):
		"""
		Defines how to handle an error when it occurs.
		"""

		if 'error: No credentials found for chain "%s"' % self.name in self.resp:
			user_msg = 'The chain "%s" does not exist' % self.name
			return ReaderSignal(user_msg, 'ERROR')

		if "error: couldn't connect to server":
			user_msg = 'Chain is not running or does not exist'
			return ReaderSignal(user_msg, 'ERROR')

	def handle_event(self):

		if 'Network parameter set was successfully generated' in self.resp:
			user_msg = 'Chain created...'
			return ReaderSignal(user_msg, 'OK')

		if '/root/.multichain/%s/params.dat already exists' % self.caller.name:
			user_msg = 'Chain already exists...'
			return ReaderSignal(user_msg, 'OK')

		if 'Multichain server starting' in self.resp:
			user_msg = 'Chain process starting...'
			return ReaderSignal(user_msg, 'OK')

		if 'Probably multichaind for this network is already running' in self.resp:
			user_msg = 'Chain process already already running...'
			return ReaderSignal(user_msg, 'OK')

class BaseCaller():

	def __init__(self, name):
		self.reader = BaseReader(self)
		self.name = str(name)

	def call(self, cmd):
		self.cmd = cmd
		return self.reader.read(run(self.cmd)) #Run command and get the reader's response


	def call_init(self, cmd):
		# Run as a sub-process so the process is not terminated before the
		#chain is initialized
		try:
			print 'Initializing blockhain...'
			process = Popen(['sudo', '/usr/local/bin/multichaind', self.name , '-daemon'], stdin=PIPE, stdout=PIPE)
			time.sleep(1)
			self.get_chain_info()
			process.stdin.close()
			print 'Blockchain initialized...'
			
		except:
			pass


	def get_response(self):
		return self.reader.get_response()

	def create_chain(self):
		"""
		Creates a new chain using the following multichain command:
			multichain-util create [name]

		"""
		cmd = 'multichain-util create %s' % self.name
		self.reader.inst = 'CREATE_CHAIN'
		return self.call(cmd)

	def initialize_chain(self):
		"""
		Initializes a newly created or stopped chain using the following multichain command:
			multichaind [name] -daemon
		"""

		cmd = 'multichaind %s -daemon' % self.name
		return self.call_init(cmd)

	def get_chain_info(self):
		"""
		Retrieves critical information about the chain using the following multichain command:
			multichain-cli [name] getinfo

		Returns the following tuple of information:
		([reader_response], [reader_payload])

		"""

		cmd = 'multichain-cli %s getinfo' % self.name
		reader_response = self.call(cmd)

		if self.reader.payload:
			return (reader_response, self.reader.payload)
		return False

	def create_account(self):

		cmd = 'multichain-cli %s getnewaddress' % self.name
		return self.call(cmd)
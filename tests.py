from datetime import timedelta
import random


from django.test import TestCase, SimpleTestCase
from django.core.urlresolvers import reverse_lazy
from django.utils import timezone
from autofixture import AutoFixture

from web_blocknet.models import PRD, PDDD
from extra import get_range_dict, Curve, ExcelReader 



# strftime('%d/%m/%Y')

class TestCurveObj(object):

	def __init__(self, created):
		self.created = created


test_objects = []
now = timezone.now()
range_dict = get_range_dict()
periods = range_dict.keys()
resp_dict = {}

test_tot = 0
for i in range(200):
	for multiple in [2,3,5,7, 11, 13, 17, 19, 23, 29]:
		test_objects.append(TestCurveObj(created = now - timedelta(days = multiple*i)))







class ExtraModuleTestCase(SimpleTestCase):

	def test_excel(self):
		e = ExcelReader()
		e.read('/home/catherine/Desktop/2.xlsx')

		#Make sure only one PRD with the target code exists
		all_prds = PRD.objects.filter(code = 345)
		if len(all_prds) > 1:
			for p in all_prds:
				p.delete()
			test_prd = AutoFixture(PRD).create(1)[0]
			test_prd.code = 345
			test_prd.save()

		lst = e.generate_model(
			'DealReceivable',
			lookup = {'PRD':('Code','code', 'prd')}
		)
		self.assertEqual(len(e.generate_model('DealReceivable')), 5)


	def test_curve(self):
		c = Curve(test_objects)
		self.assertEqual(c.data, None)
		self.assertEqual(c.display_label, '') 
		self.assertEqual(c.obj_list, test_objects)

		#See that make works with default settings...
		c.make()
		self.assertEqual(c.data, [53, 138, 91, 284, 1434])

		#Check make works with key-word argumets set...
		new_dict = c.get_range_dict(divisions = [0,4,12,26,52])
		c.make(divisions = 'weeks', range_dict = new_dict)
		self.assertEqual(c.data, [49, 85, 151, 1435, 280])
		self.assertEqual(c.data_labels, ['0-4 weeks', '4-12 weeks', '12-26 weeks', '26-52 weeks', '52+ weeks'])

		#Check that errors are raised...
		c = Curve(test_objects)
		with self.assertRaises(AttributeError):
			c.make(divisions = 'weeks', range_dict = new_dict, range_field_name = 'slipperytortoise')


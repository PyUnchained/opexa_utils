from collections import OrderedDict
from datetime import datetime, timedelta
from decimal import Decimal
from pytz import timezone as pytimezone
from random import randint
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.pdfgen import canvas
from reportlab.platypus import Table, TableStyle, SimpleDocTemplate, Paragraph, Spacer
import importlib
import os
import pyexcel as pe
import pyexcel.ext.xls # import it to handle xls file
import pyexcel.ext.xlsx
import random
import reportlab
import StringIO
import subprocess
import sys
import timeit
from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist, FieldDoesNotExist, ValidationError
from django.db.models.fields.related import ManyToManyField
from django.db.transaction import atomic
from django.db import IntegrityError
from django.http import Http404
from django.utils import timezone
from django.utils.translation import ugettext as _

from autofixture import AutoFixture

TWOPLACES = Decimal(10) ** -2

def rounded_to(num, places):
	PLACES = Decimal(10) ** -places
	return num.quantize(PLACES)


class CommonEqualityMixin(object):

	def changed_fields(self):
		return self.__dict__

	def __eq__(self, other):
		return (isinstance(other, self.__class__) and self.__dict__ == other.__dict__)
		

	def __ne__(self, other):
		return not self.__eq__(other)

class GGraph(CommonEqualityMixin):
	def __init__(self, data, chart_type, title = ' ', x_label = '', y_label = '',
		height = 300, width = 400, sub_title = ' ', pie_mapping = ' '):
		self.data = data
		
		self.title = title
		self.id = '_'.join(self.title.split(' '))
		self.chart_type = chart_type
		self.x_label = x_label
		self.y_label = y_label
		self.height = height
		self.width = width
		self.sub_title = sub_title
		self.pie_mapping = pie_mapping
		self.map_pie_data()

	def map_pie_data(self, mapping = None):
		"""If this is to be a pie chart, map the data to the correct label defined in the mapping dict."""
		if mapping:
			self.pie_mapping = mapping

		if self.pie_mapping and (self.chart_type == 'pie'):
			for mapping in self.pie_mapping:
				for data in self.data:
					if mapping[0] == data[0]:
						data[0] = mapping[1]


	def __str__(self):
		return 'GGraph ' + self.title

class GChartGenerator(object):

	def __init__(self, *args, **kwargs):

		self.graphs = []
		self.date_format = '%d/%m/%y'
		

	def set_obj_list(self, obj_list):
		"""Sets the object list and counts the total number of items."""

		self.obj_list = obj_list
		self.total_objects = len(obj_list)

		self.all_fields = self.obj_list[0].__dict__.keys()
		self.field_display_mapping = {}

		for f in self.all_fields:
			try:

				verbose_name = self.obj_list[0]._meta.get_field_by_name(f)[0].verbose_name

				#Split the name if there's a '_', then capitalize each one
				if '_' in verbose_name:
					words = verbose_name.split('_')
					for w in words:
						w = w.capitalize()
					verbose_name = ' '.join(words)

				#just capitalize if one word
				else:
					verbose_name = verbose_name.capitalize()

				#Add to mapping...
				self.field_display_mapping[f] = [verbose_name, 0]
			except FieldDoesNotExist:

				#Remove any field that can't be found on the object (like '_state' field)
				self.all_fields.remove(f)

	def create_graph(self, *args, **kwargs):
		"""Adds a graph to the list of graphs the generator instance knows about."""

		new_graph = GGraph(*args, **kwargs)
		if new_graph not in self.graphs:
			self.graphs.append(new_graph)
		return True

	def sort_dict(self, dict_x, by = 'value', reverse = True):
		import operator
		if by == 'value':
			sorted_x = sorted(dict_x.items(), key=operator.itemgetter(1))
		elif by == 'key':
			sorted_x = sorted(dict_x.items(), key=operator.itemgetter(0))

		if reverse:
			sorted_x.reverse()
		
		return self.tuple_list_to_list(sorted_x) #Convert the list of tuples to a list of lists...

	def get_periods(self, **kwargs):
		"""
		Returns a list of all the periods that must be searched through.

		time_period - gives, in days, a specific time period within which the results must fall.

		segments - defines the number of segments to split the time period into.

		"""
		time_period = kwargs.pop('time_period')

		date_format = self.date_format
		period_info = time_period.split('_')
		period_num = int(period_info[0])
		period_name = period_info[1]
		segments = 12

		#Adjust settings for yearly display
		if period_name == 'year':
			time_period = 365 * period_num
			if period_num == 1:
				date_format = '%m'
			elif period_num > 2 and period_num <=6:	#Adjust the number of segments used...
				segments = period_num * (6 - period_num + 2)
				date_format = '%d/%m'

		#Adjust setting for month display
		elif period_name == 'month' and period_num <= 12:
			time_period = 30 * period_num
			date_format = '%d/%m'
			if period_num == 1:
				segments = 6
			elif period_num > 2 and period_num <6:	#Adjust the number of segments used...
				segments = period_num * (6 - period_num + 2)
			elif period_num >= 6 and period_num <= 12:
				segments = period_num

		#Adjust settings for week display
		elif period_name == 'week' and period_num <= 4:
			time_period = 7 * period_num
			if period_num <= 2:
				segments = 7 * period_num
			elif period_num >= 3 and period_num <= 4:
				segments = 2 * period_num


		#Determine the time period in days based on

		#Split the time period into distinct segments.
		periods = []
		now = timezone.now()
		start_date = localize_date(now - timedelta(days = time_period))
		segment_unit = float(time_period)/float(segments)


		#Calculate the start and end of each segment...
		for s in range(segments):
			if s == 0:
				s_start = start_date
				s_end = localize_date(s_start + timedelta(days = segment_unit))
			else:
				s_start = localize_date(start_date + timedelta(days = (segment_unit*s)))
				if s == segments - 1:
					s_end = now		#If it's the last segment, make sure it ends at the current time
				else:
					s_end = localize_date(s_start + timedelta(days = segment_unit))

			delta = (s_end - s_start).days
			periods.append({'start':s_start, 'end':s_end,
				'delta':delta, 'period_number': s+1,
				'display_name': s_end.strftime(date_format)})

		return periods


	def get_resp(self, field, accept_none = True):
		"""Queries the specified field to find all the possible values that a field can have.
		By default, a dictionary if returned where the key is the exact value of the field,
		and a value of zero.

		"""
		resp = {}

		#Get the name of each possible value of the field and set it as the resp dict key
		for obj in self.obj_list:
			val = getattr(obj, field)
			if (val not in resp.keys()) and (val != None):
				resp[val] = 0
		return resp

	def tuple_list_to_list(self, tuple_list):
		"""
		Converts a list of tuples to a list of lists.
		"""
		as_list = []
		for tuple_item in tuple_list:
			list_item = [tuple_item[0], tuple_item[1]]
			as_list.append(list_item)
		return as_list


	def average(self, field, places = 0, **kwargs):

		periods = self.get_periods(field)



	def sum(self,field, **kwargs):
		"""
		Totals the values of the field specified.
		"""

		if 'date_field' in kwargs:
			date_field = kwargs['date_field']
		else:
			date_field = 'created'


		periods = self.get_periods(**kwargs)
		period_totals = {}
		for p in periods:
			period_totals[p['period_number']] = 0

		#Find objects that fall within the desired period...
		for obj in self.obj_list:
			date = getattr(obj, date_field)
			for p in periods:
				if (date > p['start']) and (date < p['end']):
					period_totals[p['period_number']] += getattr(obj, field)
					break

		#Convert dictionary of period totals to a list...
		totals_list = self.sort_dict(period_totals, by = 'key', reverse = False)

		#Convert the period number stored with the list to the display name
		for t_list in totals_list:
			for p in periods:
				if t_list[0] == p['period_number']:
					t_list[0] = p['display_name']

		return totals_list






	def __count(self, field):
		"""Counts the number of times"""

		for obj in self.obj_list:
			key = getattr(obj, field)
			if key != None:
				resp[getattr(obj, field)] += 1


	def count_occurences(self, field, reverse = True, num = None):
		"""
		Counts the number of times all the possible values in a field occur.
		"""

		if type(field) == type(list):
			resp = self.get_resp(field)
			return



		resp = self.get_resp(field)

		for obj in self.obj_list:
			key = getattr(obj, field)
			if key != None:
				resp[getattr(obj, field)] += 1

		#Convert response into a list of tuples, arranged by value in ascending or descending
		#order
		resp = self.sort_dict(resp, reverse = reverse)

		#If a max number of values was returned, only return the desired slice.
		if num:
			if len(resp) > num:
				return resp[:num]
			else:
				return resp

		#Else, return the whole list
		return resp

def timeit_wrapper(func, *args, **kwargs):
	def wrapped():
		return func(*args, **kwargs)
	return wrapped





@atomic
def save_many(objs):
	for item in objs:
		try:
			with atomic():
				item.save()
		except IntegrityError:
			pass

def localize_date(dt, tz = settings.TIME_ZONE):
	#Set time to midninght
	new_dt = datetime(dt.year,dt.month,dt.day,0,0,0,0)
	current = pytimezone(tz)
	return current.localize(new_dt)




def random_with_N_digits(n):
		range_start = 10**(n-1)
		range_end = (10**n)-1
		return randint(range_start, range_end)


def merge_dicts(*dict_args):
		'''
		Given any number of dicts, shallow copy and merge into a new dict,
		precedence goes to key value pairs in latter dicts.
		'''
		result = {}
		for dictionary in dict_args:
				result.update(dictionary)
		return result

class OPEXAValueError(ValueError):
		'''Raised when there's an error with the submitted value '''


class ExcelReader():

	def read(self, file_name):
		self.records = pe.get_records(file_name=str(file_name))

	def generate_model(self, class_name, module_name = 'web_blocknet.models', context = {},
		lookup = None, convert_float = True, save = False):
		"""
		Generates Django models from the information in the excel sheet.

		[class_name] - Name of the model class to create as a string
		[module_name] - Name of the module where the class is defined as a string
		[context] - dictionary where the key is the name of the a field, and the value represents
								the value for the field to use for ALL models created.
		[lookup] - Sometimes the excel file will contain references to other models which should already
							 exist in the database. This is a dictionary where the key is the name of a model, and
							 the value is a tuple in the form ([excel_column_heading],[corresponding_model_field]).
							 The function will first make sure that the corresponding instance of the defined model
							 already exists in the database before creating the new objects.
		[convert_float]		- Boolean determining whether or not to convert float values into Decimals.
		[save]		- Boolean determining whether or not to save the newly created objects before
								returning the list.
		"""
		new_objects = []

		#Import the class object from the specified module
		module = importlib.import_module(module_name)
		class_ = getattr(module, class_name)
		instance = class_()
		relationships = instance.excel_relationships()

		#Lookup whether the other objects required to create the object exist
		if lookup:
			for k in lookup.keys():
				try:
					#Try get the desired model...
					lookup_class = getattr(module, k)
					db_query_param = lookup[k][1]

					#Extract all possible values in the desired field...
					possible_values = []
					count = 0
					for rec in self.records:
						count += 1
						field_value = rec[lookup[k][0]]
						if (field_value, 'row {0}'.format(count)) not in possible_values:
							possible_values.append((field_value, 'row {0}'.format(count)))

					#Attempt to retrieve each of the specific objects
					not_recognized = []
					for v in possible_values:
						kwargs_dict = {db_query_param:v[0]}
						try:
							lookup_class.objects.get(**kwargs_dict)
						except ObjectDoesNotExist:
							not_recognized.append(v) #List together all the objects that could not be found

					if len(not_recognized) > 0:
						response_txt = ''
						for item in not_recognized:
							txt = '{0} in {1},'.format(item[0], item[1])
							response_txt = response_txt + txt
						return 'Some information in the file you have uploaded is incorrect. Please correct the information and upload again:\n{0}'.format(response_txt) 
				except KeyError:
					pass


		

		#Convert each record from the excel file into an instance of the desired object.
		for rec in self.records:
			

			relationship_dict  = context

			for k in rec.keys():

				#Add the objects that match those required in the lookup in the kwarg dict for creating a new
				#object....
				if lookup:
					for lookup_k in lookup.keys():
						if lookup[lookup_k][0] == k:
							lookup_class = getattr(module, lookup_k)
							get_kwargs = {lookup[lookup_k][1]:rec[k]}
							lookup_obj = lookup_class.objects.get(**get_kwargs)
							relationship_dict[lookup[lookup_k][2]] = lookup_obj
						

				#Add relationships between column headings and the model fields
				for rel in relationships:
					if k in rel:
						#Convert float values to Decimal objects
						if (type(rec[k]) == float) and (convert_float == True):
							relationship_dict[rel[1]] = Decimal(str(rec[k]))
						else:
							relationship_dict[rel[1]] = rec[k]

			#Determine whether to immediately save created objects
			if save:
				new_obj = class_(**relationship_dict).save()
			else:
				new_obj = class_(**relationship_dict)
				
			new_objects.append(new_obj)

		return new_objects
			

def to_dict(instance):
		opts = instance._meta
		data = {}
		for f in opts.concrete_fields + opts.many_to_many:
				if isinstance(f, ManyToManyField):
						if instance.pk is None:
								data[f.name] = []
						else:
								data[f.name] = list(f.value_from_object(instance).values_list('pk', flat=True))
				else:
						data[f.name] = f.value_from_object(instance)
		return data

def create_dict(kv_list):
		d = OrderedDict()
		for item in kv_list:
			d[item[0]] = item[1]
		return d

class StatBall():

	def __init__(self, stats_list):
		self.stats = stats_list
		self.fields = []
		self.rel_dict = {}
		self.graph_prefix = None
		self.graph_suffix = None

	def clone(self):
		return StatBall(self.stats)


	def make_curves(self):
		pass

	def set_fields(self):
		for la in self.stats[0].keys():
			
			w = la.split('_')
			index = 0
			for cha in w:
				w[index] = cha.capitalize()
				index += 1
			display_name = ' '.join(w)
			self.fields.append(display_name)
			self.rel_dict[la] = display_name

	def return_stats(self, ball):
		return [self, ball]

	def top(self, num, by):
		#Every function should start by setting the fields and display names to use
		self.set_fields()

		name_lst = []
		value_lst = []
		for st in self.stats:
			name_lst.append(st['verbose_name'])
			value_lst.append(st[by])

		top_lst = []
		for i in range(num):
			if len(value_lst) == 0:
				break

			if (len(value_lst) == 1) and (len(name_lst) == 1):
				top_lst.append((name_lst[0],value_lst[0]))
				del name_lst[0]			#Delete name and corresponding value from list of available
				del value_lst[0]
				break

			#Get the highest value and its index
			value = max(value_lst)
			index = value_lst.index(value)

			top_lst.append((name_lst[index],value)) 	#Add value to the list
			del name_lst[index]			#Delete name and corresponding value from list of available
			del value_lst[index]

		self.graph_prefix = 'Top {0} '.format(num)
		self.graph_suffix = 'by {0}'.format(self.rel_dict[by])


		return self.return_stats(create_dict(top_lst))

		
class Curve():

	def __str__(self):
		return str(self.data) + ' ' + self.display_label

	def __init__(self, data):
		#Newer behaviour being implemented
		self.data = []
		self.stats_ball = data[0]
		self.graph_labels = self.stats_ball.fields
		self.data_dict = data[1]
		self.data_labels = []
		for k in data[1]:
			self.data_labels.append(k)
			val = float(data[1][k])		#Convert number to float because Decimals won't work with the Javascript
			self.data.append(val)

		

	def graph_title(self, model_name = None):
		if model_name:
			return self.stats_ball.graph_prefix + str(model_name) + ' ' + self.stats_ball.graph_suffix
		else:
			return self.stats_ball.graph_prefix + self.stats_ball.graph_suffix

	def make(self, *args, **kwargs):

		if 'display_label' in kwargs:
			self.display_label = kwargs.pop('display_label')

		if 'no_range' not in kwargs:
			self.get_within_range(*args, **kwargs)
		else:
			pass


		return self



	def get_within_range(self, range_field_name = 'created', from_date = timezone.now(),
		range_dict = None, divisions = 'days', fields = None, **kwargs):
		"""
		Returns all results that fall within a certain range on a DateTime field.

		[self.obj_list] - List of the model objects to separate
		[range_field_name] - Name of the DateTime field in the model
		[from_date] - Date to start searching back from_date
		[range_dict] - An ordered dictionary with a numerical value for the periods to search in
		[divisions] - determines the keyword to use with the timedelta function.
		[fields] - A dictionary in the form {'field_name':'desired_value'}
		"""
		self.data_labels = []

		if range_dict == None:
			range_dict = self.get_range_dict()

		resp_dict = {}
		dict_keys = range_dict.keys()


		#Split range dict into periods...    
		for i in range(len(dict_keys)):
			try:
				beginning = dict_keys[i]
				end = dict_keys[i+1]
				key_str = '{0}-{1}'.format(beginning, end)
				resp_dict[key_str] = 0

			#When it is the last entry in the periods, simply assume it is from this time onwards...
			except IndexError:
				beginning = None
				end = dict_keys[-1]
				key_str = '{0}+'.format(end)
				resp_dict[key_str] = 0


			self.data_labels.append('{0} {1}'.format(key_str,divisions))
			#Format the kwargs that will dynamically assign kwargs in timedelta function
			end_kwargs = {divisions:end}
			beginning_kwargs = {divisions:beginning}

			for obj in self.obj_list:   
				obj_date = getattr(obj, range_field_name)

				#look for an object with a date older than the beginning time, but earlier than the end time...
				if obj_date: #Excludes cases where the field has a null value
					if beginning:
						if (from_date - timedelta(**end_kwargs) < obj_date) and (from_date - timedelta(**beginning_kwargs) >= obj_date):
							resp_dict[key_str] += 1

					#If no beginning, either it's Zero or undefined
					else:
						if beginning == 0:
							#look for an object with a date newer than the end date of the period..
							if (from_date - timedelta(**end_kwargs) < obj_date):
								resp_dict[key_str] += 1

						#Assume beginning undefined and look for objects older than the given date...
						elif (from_date - timedelta(**end_kwargs) >= obj_date):
							resp_dict[key_str] += 1

		#Save data as a list and as a dictionary
		self.data = []
		for k in resp_dict:
			self.data.append(resp_dict[k])
		self.data_dict = resp_dict

	def get_range_dict(self, divisions = [0,30,90,180,365]):
		
		d = OrderedDict()
		for div in divisions:
			d[div] = 0

		return d

class Graph():

	def __init__(self, title, curves, graph_type = 'Bar', ):
		self.title = title
		self.id = 'g_' + convert_to_id(title)
		self.curves = curves
		self.data_set_colors = ['#5B90BF', '#06c318', '#F27009',
			'#bc0859','#8a1414','#8d3de7', '#b0f952', '#f4bf62','#138254', '#3c5357', '#e596a5']
		self.labels = curves[0].data_labels
		self.width = 300
		self.height = 300
		self.graph_type = graph_type




class StatsTemplate():

	def __init__(self, title, graph = False):
		self.title = title
		self.id = 'g_' + convert_to_id(title)

def convert_to_id(title):
	new_title = title.replace(' ','_')
	new_title = new_title.replace("'",'_')
	return new_title

def get_within_range(obj_list, field_name = 'created', from_date = timezone.now(), range_dict = None, divisions = 'days'):
	"""
	Returns all results that fall within a certain range on a DateTime field.

	[obj_list] - List of the model objects to separate
	[field_name] - Name of the DateTime field in the model
	[from_date] - Date to start searching back from_date
	[range_dict] - An ordered dictionary with a numerical value for the periods to search in
	[divisions] - determines the keyword to use with the timedelta function.
	"""
	
	if range_dict == None:
		range_dict = get_range_dict()

	

	for obj in obj_list:
		obj_date = getattr(obj, field_name)

		for k in range_dict:
			func_kwargs = {divisions: k} #Dynamically assign the keyword argument used by timedelta based on the divisions variable
			if obj_date > from_date - timedelta(**func_kwargs):
				range_dict[k] += 1

	#Turn the data into a list
	as_list = []
	for k in range_dict:
		as_list.append(range_dict[k])

	return as_list

def make_list_boolean(obj_list, attribute, add = True):
	output = []
	for o in obj_list:
		try: 
			if getattr(o, attribute) == add:
				if o not in output:
					output.append(o)

		except ObjectDoesNotExist:
			if add:
				try:
					if getattr(o, attribute) != None:
						if o not in output:
							output.append(o)
				except ObjectDoesNotExist:
					pass

			else:

				try:
					if getattr(o, attribute) != None:
						pass
				except ObjectDoesNotExist:
					if o not in output:
							output.append(o)

	return output



def get_range_dict(divisions = [0,30,90,180,365]):
	if 0 not in divisions:
		divisions.append(0)

	d = OrderedDict()
	for div in divisions:
		d[div] = 0

	return d
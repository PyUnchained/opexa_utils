from extra import to_dict
import ast
import socket
import sys
import select
from pexpect import run, spawn
import subprocess
import re
from threading  import Thread
from Queue import Queue, Empty
from subprocess import PIPE, Popen
from threading  import Thread
import time
from shlex import split
import json

class CommandLineInterface():

	def execute(self):
		return getattr(self, self.cmd)()

	def start(self, cmd):
		"""
		Simply starts a process, and returns the pid of the new running process.

		[cmd] - string of the command as it would normally be typed into the terminal.
		"""
		if '|' in cmd:
			p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell = True)
		else:
			p = subprocess.Popen(split(cmd), stdout=subprocess.PIPE)
		return p.pid

	def run_communicate(self, cmd):
		"""
		Runs a terminal command and returns the output from the command.

		[cmd] - string of the command as it would normally be typed into the terminal.
		"""

		#If there's a pipe character in the command, it must be openned with the shell interpreter to work properly
		if '|' in cmd:
			p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell = True)
		else:
			p = subprocess.Popen(split(cmd), stdout=subprocess.PIPE)

		output, err = p.communicate()
		print output
		return output


class ModelMixin(object):
	"""
	Adds some common, usefull functionality to all model classes.
	"""
	
	def get_info(self):
		return to_dict(self)


	def list_of_dicts(self, lst):
		"""
		Converts a list of python objects into a list of those objects represented as
		dictionaries.
		"""

		if len(lst) > 0:
			output_list = []
			for obj in lst:
				output_list.append(to_dict(obj))
			return output_list

		else:
			return []
		
		


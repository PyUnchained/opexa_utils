from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
from django.db import IntegrityError
from django.db import models
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.utils.html import format_html
from django.utils.safestring import SafeText
from django.core.validators import MinValueValidator
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.db.models import Q

from picklefield.fields import PickledObjectField

def initialize_notification(obj):
	model_class = obj.__class__
	original_pk = obj.pk

	return ChangeNotification(model_class = model_class,
		original_pk = original_pk)


class ChangeNotification(models.Model):
	model_class = models.PickledObjectField()
	original_pk = models.CharField(max_length = 200)

	new_pk = models.CharField(max_length = 200, blank = True, null = True)
	field_changed = models.CharField(max_length = 100, blank = True,
		null = True)
	old_value = PickledObjectField(max_length = 100, blank = True,
		null = True)
	new_value = PickledObjectField(max_length = 100, blank = True,
		null = True)
	changes_pickle = PickledObjectField(blank = True, null = True)

	applied = models.DateTimeField(blank = True, null = True)
	created = models.DateTimeField(auto_now_add = True)

	notice_for = models.CharField(max_length = 100, blank = True, null = True)
	confirmation_given = models.BooleanField(default = False)

	@property
	def jquery_id(self):
		return "{0}.{1}-{2}".format(self._meta.app_label, self._meta.object_name, self.pk).lower()